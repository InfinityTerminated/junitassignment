package bearman;

import food.Food;

public class PizzaEatingBearman extends Bearman{
	public void eat(Food f) {
		f.feedBearman(this);
	}
}
